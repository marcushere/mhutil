use strict; use warnings; use File::Copy;
# perl script to iterate over different compilation settings for parcel model
# written 2/26/16 by MRH
@runs = (
'pgf90 -r8 -i8 -fast -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'pgf90 -r8 -i8 -fast -Mipa=fast,inline -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'pgf90 -r8 -i8 -fast -Mipa=fast,inline -Mconcur  -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'pgf90 -r8 -i8 -fast -Mipa=fast,inline -Mconcur -Mnofpapprox -Mnofprelaxed -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'pgf90 -r8 -i8 -fast -Mipa=fast,inline -Mconcur -Kieee  -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'pgf90 -r8 -i8 -fast -Mipa=fast,inline -Mconcur -Mnofpapprox -Mnofprelaxed -Kieee -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'ifort -fast -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'ifort -Os -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'ifort -fast -ipo -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'ifort -fast -mp1 -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'ifort -parallel -fast -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'ifort -fast -mp -oa.out vode.f ilpm_homhet_fix_demott_bex.f',
'ifort -fast -mp1 -oa.out vode.f ilpm_homhet_fix_demott_bex.f');



foreach my $params ('A','B','C','D','E','F','G','H','I','J','K','L','M'){
# flag to tell perl to overwrite output files or not
# 0: skip existing files, go to next run
# 1: run all simulations, overwrite
my $foverwrite = 1;
# flag to tell how much ice to include on the two plates (assumes
# the chamber is symmetric)
# 0: use normal model (no ice, 1.3 cm profile)
# 1: include 1.9 mm thick ice (1.08 cm)
# 2: include 0.9 mm thick ice (1.18 cm)
# 3: include 1.5 mm thick ice (1.12 cm) (see %nzvals below)
foreach my $wice (0,1,2,3){
#my $wice = 0;
# name of log file
my $logfile = "loopProfileLog.txt";
# fortran compiler argument list
my @pgf90args = ('-r8','-i8','-O4','-Mvect=nosimd','-Mipa=fast,inline');
# index file with information about data and experiments
my $idxfile = "../../data/index.dat";
# fortran source files
my $srcfile = "";
my $incfile = "paramsthermal.inc";
if ($wice>0) {
    $srcfile = "thermalgrad8wthring-wice.f90";
    $incfile = "paramsthermal-wice.inc";
} else {
    $srcfile = "thermalgrad8wthring-auto.f90";
}
# file to write new fortran to
my $newsrcfile = ".tmp.$srcfile";

# jerry's wdt values
my %wdt_j0 = ('wdt','50','wdt2','41','wdt3','25','op','0'); # standard (guess)
my %wdt_j1 = ('wdt','63','wdt2','41','wdt3','25','op','1'); # wide opening
my %wdt_j2 = ('wdt','44','wdt2','41','wdt3','25','op','2'); # narrow opening
# my wdt values
my %wdt_m0 = ('wdt','50','wdt2','41','wdt3','25','op','0'); # standard (guess)
my %wdt_m1 = ('wdt','60','wdt2','30','wdt3','26','op','1'); # wide opening (dry)
my %wdt_m2 = ('wdt','48','wdt2','30','wdt3','12','op','2'); # narrow opening (wet)
# nz values
# controls chamber height
my %nzvals = ('z0','64','z1','54','z2','59','z3','56');
# 64 => 64*(0.2 mm) = 12.8 mm
# 54 => 54*(0.2 mm) = 10.8 mm (1.9 mm ice)
# 59 => 59*(0.2 mm) = 11.8 mm (0.9 mm ice)
# 56 => 56*(0.2 mm) = 11.2 mm (1.5 mm ice)

# begin program
# open index file
open my $in,  '<',  $idxfile      or die "Can't read index file: $!";

<$in>; # read "header" line

# subroutine to trim leading and trailing whitespace off of a string
sub trim {my $s=shift; $s=~s/^\s+|\s+$|\r//g; return $s;};

my @repvalsarr = ();

DXLINE: while( <$in> ){
    my $line = trim($_);
#    next IDXLINE if !($line =~ /20160211/);
    my @spl = split /[, ]+/;
    my $date = $spl[0];
    my $time = $spl[1];
    my $ltmp = $spl[3];
    my $utmp = $spl[4];
    my $idx = $.-1;
    my $nzval = $nzvals{"z$wice"};
    my %rvals = ('date',"$date",
		 'ttopc',"$utmp",
		 'tbotc',"$ltmp",
		 'time',"$time",
		 'idx',"$idx",
		 'z',"$nzval");
    push(@repvalsarr,\%rvals);
}
close $in;

# list to hold the wdt hashes
my @wdtvals = ();

# choose one of the sets of values to use based on flag set above
if ($jerry_wdt_vals) {
    @wdtvals = (\%wdt_j0,\%wdt_j1,\%wdt_j2);
} else {
    @wdtvals = (\%wdt_m0,\%wdt_m1,\%wdt_m2);
}

my %prevtemps = ();

# build the include file first (this is the same the whole time)
my $incfileout = ".$incfile";
open my $incin, '<', $incfile or die "Couldn't open include file: $?\n";
open my $incout, '>', $incfileout or die "Couldn't open include output file: $?\n";
while(<$incin>) {
    my $line = trim($_);
    if ($line =~ /!paramNZ/g) { # !paramNZ indicates the line with nz
	my $nz = $nzvals{"z$wice"}; # put correct nz value in include file
	$line =~ s/nz=\d+/nz=$nz/ig;
    }
    if (($line =~ /\S+/) and !($line =~ /^(!|\s+!)/)) {
	# print line to new file if not just whitespace
	print {$incout} "$line\n";
    }
}
close $incin;
close $incout;

# open log file
open my $log, '>', $logfile or print "Coudn't open logfile: $?\n";

# use hash %repvals to hold arguments for the current run
foreach my $idxvals (@repvalsarr) {
    foreach my $wdt (@wdtvals) {
	# assemble hash of values for this run
	my %repvals = (%{$idxvals},%{$wdt});
	# assemble name for output file
	my $date = $repvals{'date'}; my $time = $repvals{'time'};
	my $filetag = sprintf("%08u-%06u",$repvals{'date'},$repvals{'time'});
	my $fidx = $repvals{'idx'};
	my $opening = $repvals{'op'};
#	$opening = ($wice) ? $opening+3 : $opening;
	$opening = $wice*3+$opening;
 	my $newoutputfile = "$filetag-prof-$opening.dat";
	if ($jerry_wdt_vals) { # append a 'j' if using Jerry's grid values
	    $newoutputfile = "$filetag-prof-${opening}j.dat";
	}
	# don't run if file already exists
	next if (-e $newoutputfile and $foverwrite==0);
	# start doing actual work
	print join ' ',%repvals,($wice ? 'wice' : ''),"\n$newoutputfile\n"; # print current run
	print {$log} join ' ',%repvals,($wice ? 'wice' : ''),"\n$newoutputfile\n"; # print to logfile
	# open fortran file
	open $in,  '<',  $srcfile      or die "Can't read program: $!";
	open my $out, '>', $newsrcfile or die "Can't write temporary code: $!";
	# read through top of file and replace the appropriate bits
	# with the values in %repvals
	print {$log} "writing fortran file\n"; # print to logfile
	while( <$in> ) # read line
	{
	    my $line = trim($_); # trim trailing/leading spaces
	    if ($line =~ /\S+\s+\S*/) { # if not an empty line
		my @spl = split /\s+/,$line; # split line by whitespace
		if (exists $repvals{$spl[0]}) {
		    # if first word is a key in repvals, print
		    # replacement into new file
		    print {$out} "$spl[0] = $repvals{$spl[0]}\n";
		} elsif($line =~ /!incl/ig) {
		    print {$out} "include '$incfileout'\n"
		} else {
		    # otherwise, just print existing line into new file
		    print {$out} "$line\n";
		}
	    } else { # print line into file if not just whitespace
		print {$out} "$line\n" if ($line =~ /\S/);
	    } # if this is line number 200, exit loop
	    last if $. == 200;#$spl[0] =~ /~! end/;
	}
	# all of the variables are written in the top portion of the file
	while ( <$in> ){ # read line
	    my $line = trim($_);
	    if ($line =~ /!paramNZ/g) {
		my $nz = $repvals{'z'}+1; # n2=nz+1 (in MPDATA2D)
		$line =~ s/n2=\d+/n2=$nz/ig;
	    }
	    if (($line =~ /\S+/) and !($line =~ /^(!|\s+!)/)) {
		# print line to new file if not just whitespace
		print {$out} "$line\n";
	    }
	}
	close $in;
	close $out;
	print {$log} "finished writing fortran file\n"; # print to logfile

	print {$log} "compiling fortran source\n"; # print to logfile
	# assemble compilation command and compile program
	my @cmd = ('pgf90',$newsrcfile,'-orun.out',@pgf90args);
	if (system(@cmd)) {
	    die "fortran compiler failed: $?\n";
	} else {
	    print "compiled successfully.\n";
	    print {$log} "finished compiling\n"; # print to logfile
	}
#	unlink $newsrcfile;
	
	print {$log} "running program\n"; # print to logfile
	# create command and run fortran program
	@cmd = ('./run.out');
	if (system(@cmd)) {
	    die "program failed: $?\n";
	} else {
	    print "program ran successfully\n";
	    print {$log} "program ran successfully\n"; # print to logfile
	}

	my $outputfile = "outwthring/profiles-center-nx-time5.dat";
	# rename data output file to meaningful name
	# so it won't be overwritten
	print "New output file: $newoutputfile.\n";
	if (rename ($outputfile, $newoutputfile)==0) {
	    print {$log} "COULDN'T COPY OUTPUT FILE\n"; # print to logfile
	    die "Couldn't rename file: $?\n";
	}
	print {$log} "output file moved to $newoutputfile\n"; # print to logfile
	# now copy to data directory
	my $newfname = "/ortley/s0/mrh318/cloudlabresearch/data/$date/$time/$newoutputfile";
	print "$newoutputfile ",copy($newoutputfile,$newfname),"\n";

    }
}
}
}
