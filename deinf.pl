#!/usr/bin/perl -sw
use strict; use warnings;

=pod
    a perl script to remove all of the NaN's and inf's from a data file, replacing
    them with 0
=cut

# checks that file exists, dies if not
sub checkfile{return (-f shift)};

# subroutine to trim leading and trailing whitespace off of a string
sub trim {my $s=shift; $s=~s/^\s+|\s+$|\r//g; return $s;};

my $hasinf = 0;
my $hasnan = 0;
my $infval = '00.0'; # value to replace inf
my $nanval = '0.00'; # value to replace nan

my $datname = $ARGV[0];
print "$datname: \n";
# make sure it's actually a file
checkfile($datname) or die "File not found: $datname\n";;

# open data file
open(my $dat, '<', $datname) or die "Couldn't open file: $datname, $?\n";

# check that it looks like a data file
my $line = <$dat>;
#if (not ($line =~ /.\d+./))
#{
#    print "Error: $datname is not a Grace project file\n";
#    goto DONE;
#}

# open tmp file to copy to
my $outfname = ".tmp.$datname";
#while (checkfile($outfname)){$outfname = ".a$outfname"};
open(my $outf, '>', $outfname) or die "Couldn't open temporary file: $?\n";
print {$outf} $line;

# loop through data file
while ($line=<$dat>)
{
    $line = trim($line);
    if ($line =~ /inf|infinity|infty/gi) # check for inf
    {
	$hasinf++;
#	print "$., inf line: $hasinf\n";#$line\n";
	$line =~ s/infinity|infty|inf/$infval/gi;
#	print "$line\n\n";
    } 
    if ($line =~ /nan/gi) # now check for nan
    {
	$hasnan++;
#	print "$.,nan line: $hasnan\n";#$line\n"
	$line =~ s/nan/$nanval/gi;
    }
    # print modified line
    print {$outf} "$line\n";
}

 DONE:{
     close $dat;
     close $outf;
}

# only need to copy file if there actually were values to change
if ($hasnan+$hasinf)
{
    unlink $datname;

    open ($dat, '>', $datname) or warn "Couldn't open output file: $datname, $?\n";
    open ($outf, '<', $outfname) or warn "Couldn't open temporary file: $outfname, $?\n";

    # copy data
    print {$dat} $line while ($line = <$outf>);
}

# delete temporary file
unlink $outf;

if ($hasinf+$hasnan)
{
    print "removed $hasinf inf's and $hasnan nan's\n";
} else
{
    print "no inf or nan\n";
}
