#!/usr/bin/perl -sw
use strict; use warnings;

=pod
    a perl script to remove all of the sets from a grace project file, leaving 
    the graphs and settings untouched
=cut

# checks that file exists, dies if not
sub checkfile{return (-f shift)};

# subroutine to trim leading and trailing whitespace off of a string
sub trim {my $s=shift; $s=~s/^\s+|\s+$|\r//g; return $s;};

my $hasdata = 0;

my $grfname = $ARGV[0];
print "$grfname: ";
# make sure it's actually a file
checkfile($grfname) or die "File not found: $grfname\n";;

# open project file
open(my $grf, '<', $grfname) or die "Couldn't open file: $grfname, $?\n";

# check that it looks like a grace project file
my $line = <$grf>;
if (not ($line =~ /Grace/))
{
    print "Error: $grfname is not a Grace project file\n";
    goto DONE;
}

# open tmp file to copy to
my $outfname = ".tmp.$grfname";
#while (checkfile($outfname)){$outfname = ".a$outfname"};
open(my $outf, '>', $outfname) or die "Couldn't open temporary file: $?\n";
print {$outf} $line;

my $setflg = 0; # indicates if a set of data is being read
# loop through project file
while ($line=<$grf>)
{
    $line = trim($line);
    if ($line =~ /^\@target g\d+.s\d+/i)
    {
	$setflg++;
	#print "set line: $line\n";
    } elsif ($line =~ /^\@type /i)
    {
	$setflg++;
	#print "set type line: $line\n"
    } elsif ($line =~ /^&/i)
    {
	$setflg--; # end of a set, reset flag
	$setflg--;
    } elsif ($line =~ /^[\-0-9.]+/i && $setflg > 0)
    {
	$hasdata = 1;
	# do nothing, don't copy data
    } else
    {
	print {$outf} "$line\n";
    }
}

 DONE:{
     close $grf;
     close $outf;
}

# only need to copy file if there actually was data to remove
if ($hasdata)
{
    unlink $grfname;

    open ($grf, '>', $grfname) or warn "Couldn't open output file: $grfname, $?\n";
    open ($outf, '<', $outfname) or warn "Couldn't open temporary file: $outfname, $?\n";

    # copy data
    print {$grf} $line while ($line = <$outf>);
}

# delete temporary file
unlink $outf;

if ($hasdata)
{
    print "data removed\n";
} else
{
    print "no data\n";
}
